import Vue from 'vue';
import Vuex from 'vuex';
import events from './store_modules/events';
import event from './store_modules/event';
import general from './store_modules/general';
import userAccount from './store_modules/userAccount';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  getters: {

  },
  modules: {
    events,
    event,
    general,
    userAccount,
  },
});
